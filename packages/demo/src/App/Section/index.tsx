import React from "react";

interface Props {
	title: string;
	subtitle: any;
	dark: any;
	id: any;
}

export const Section: React.FC<Props> = ({ title, subtitle, dark, id }) => {
	return (
		<div className={"section" + (dark ? " section-dark" : "")}>
			<div className="section-content" id={id}>
				<h1>{title}</h1>
				<p>{subtitle}</p>
			</div>
		</div>
	);
};
