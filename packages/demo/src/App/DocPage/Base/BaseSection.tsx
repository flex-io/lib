import React, { ReactNode } from "react";
import PropTypes from "prop-types";
import { BasePageContext } from "./context";

export interface BaseSectionInputs {
	key: string;
	displayName: ReactNode;
}

export class BaseSection extends React.PureComponent {
	[x: string]: any;
	static propTypes: {
		displayName: React.Validator<ReactNode>;
	};

	props!: BaseSectionInputs & { children: React.ReactNode };

	componentDidMount() {
		this.context.context.addItem({
			key: this._reactInternalFiber.key,
			displayName: this.props.displayName,
		});
	}

	render() {
		const { children } = this.props;

		//Section Key
		return children;
	}
}
BaseSection.contextType = BasePageContext;
BaseSection.propTypes = {
	displayName: PropTypes.node.isRequired,
};
