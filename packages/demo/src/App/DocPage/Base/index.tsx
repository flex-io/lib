/** @jsx jsx */
import { jsx } from "@emotion/core";
import React from "react";
import { BasePageConsumer as Consumer, BasePageProvider as Provider, IBaseSection } from "./context";
import { BaseSection } from "./BaseSection";

export interface IBasePageStyle {
	_container: any;
	_menuContainer: any;
	_sectionContainer: any;
	_asideContainer: any;
}

export interface IBaseTabsProps {
	_style?: IBasePageStyle;
	renderMenu: (items: Array<IBaseSection>) => React.ReactNode;
}

export class BasePage extends React.PureComponent<IBaseTabsProps> {
	static Section: typeof BaseSection = BaseSection;
	render() {
		const { children, _style } = this.props;
		return (
			<Provider>
				<Consumer>
					{(value) => (
						<React.Fragment>
							{this.props.renderMenu(value.context.items)}
							{children}
						</React.Fragment>
					)}
				</Consumer>
			</Provider>
		);
	}
}
