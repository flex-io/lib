import React from "react";
import { BasePage } from "./Base";
import { Link } from "react-scroll";
import { BaseSection } from "./Base/BaseSection";

export class DocPage extends React.PureComponent {
	static Section = BaseSection
	render() {
		return (
			<BasePage
				renderMenu={(items) =>
					items.map((item) => (
						<Link
							activeClass="section_active"
							to={item.key}
							spy={true}
							smooth={true}
							offset={0}
							duration={500}
						>
							{item.displayName}
						</Link>
					))
				}
			>
				{this.props.children}
			</BasePage>
		);
	}
}
