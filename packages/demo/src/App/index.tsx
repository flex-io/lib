import React from "react";
import { DocPage } from "./DocPage";

// const PageSections = [
// 	{
// 		item: {displayName: "Button"},
// 		section:
// 	}
// ]

const App: React.FC = () => {
	return (
		<DocPage>
			<DocPage.Section key="Key1" displayName="Section 1">
				<div>Test</div>
			</DocPage.Section>
			<DocPage.Section key="Key2" displayName="Section 2">
				<div>Test</div>
			</DocPage.Section>

			<DocPage.Section key="Key3" displayName="Section 3">
				<div>Test</div>
			</DocPage.Section>
		</DocPage>
	);
};

export default App;
