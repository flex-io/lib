/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import React from "react";
import { Link } from "react-scroll";

const SectionLinksStyle = {
	_ul: css`
		display: flex;
		flex-direction: column;
		position: -webkit-sticky;
		position: sticky;
		top: 0;
	`,
	_li: css`
		font-weight: normal;
		-webkit-font-smoothing: antialiased;
		font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
		list-style: none;
		box-sizing: inherit;
		background-color: transparent;
		text-decoration: none;
		cursor: pointer;
		font-size: 0.85rem;
		padding-top: 0.5rem;
		padding-bottom: 0.5rem;
		color: #717171;
		line-height: 1.5;
		transition: color 0.3s ease;
	`,
};

interface ISectionLinksStyle {
	_ul: any;
	_li: any;
}

interface Props {
	_style?: ISectionLinksStyle;
}

export const SectionLinks: React.FC<Props> = ({ _style = SectionLinksStyle }) => {
	return (
		<ul css={_style?._ul}>
			<li className="nav-item" css={_style?._li}>
				<Link activeClass="section_active" to="section1" spy={true} smooth={true} offset={0} duration={500}>
					Buttton
				</Link>
			</li>
			<li className="nav-item" css={_style?._li}>
				<Link activeClass="section_active" to="section2" spy={true} smooth={true} offset={0} duration={500}>
					Section 2
				</Link>
			</li>
			<li className="nav-item" css={_style?._li}>
				<Link activeClass="section_active" to="section3" spy={true} smooth={true} offset={0} duration={500}>
					Section 3
				</Link>
			</li>
			<li className="nav-item" css={_style?._li}>
				<Link activeClass="section_active" to="section4" spy={true} smooth={true} offset={0} duration={500}>
					Section 4
				</Link>
			</li>
			<li className="nav-item" css={_style?._li}>
				<Link activeClass="section_active" to="section5" spy={true} smooth={true} offset={0} duration={500}>
					Section 5
				</Link>
			</li>
		</ul>
	);
};
