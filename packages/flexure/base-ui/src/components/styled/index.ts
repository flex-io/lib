import * as EmotionStyled from "@emotion/styled";
import { CreateStyled } from "@emotion/styled";

export interface ITheme {
	palette: {
		themePrimary: any;
		themeLighterAlt: any;
		themeLighter: any;
		themeLight: any;
		themeTertiary: any;
		themeSecondary: any;
		themeDarkAlt: any;
		themeDark: any;
		themeDarker: any;
		neutralLighterAlt: any;
		neutralLighter: any;
		neutralLight: any;
		neutralQuaternaryAlt: any;
		neutralQuaternary: any;
		neutralTertiaryAlt: any;
		neutralTertiary: any;
		neutralSecondary: any;
		neutralPrimaryAlt: any;
		neutralPrimary: any;
		neutralDark: any;
		black: any;
		white: any;
	};
}

export default EmotionStyled.default as CreateStyled<ITheme>;
