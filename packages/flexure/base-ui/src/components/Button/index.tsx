import React from "react";
import _ from "lodash";
import { ButtonStyled, IBaseButtonProps } from "./Styled";

export class BaseButton extends React.PureComponent<IBaseButtonProps> {
	constructor(props: IBaseButtonProps) {
		super(props);
	}
	render() {
		return <ButtonStyled {...this.props}>{this.props.children}</ButtonStyled>;
	}
}
