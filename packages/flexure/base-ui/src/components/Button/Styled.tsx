import _ from "lodash";
import styled, { ITheme } from "../styled";

export type IBaseButtonProps = {
	scale?: "small" | "normal" | "big";
	kind?: "primary" | "secondary" | "cancel" | "dark" | "gray";
	outline?: boolean;
	_style?: IBaseButtonStyle;
	disabled?: boolean;
};

interface IButton extends Omit<IBaseButtonProps, "_style"> {
	theme: ITheme;
	[key: string]: any;
}

export interface IBaseButtonStyle {
	_button: (props: IButton) => any;
}



export const ButtonStyled = styled("button")<IBaseButtonProps>`
	${(props) => {
		const sProps = _.omit(props, "children", "_style");
		return props._style?._button({ ...sProps });
	}}
`;

