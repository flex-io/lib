import React from "react"
import { TextInputStyled, IBaseTextInputProps } from "./Styled"

export const BaseTextInput: React.FC<IBaseTextInputProps> = (props) => {
	return <TextInputStyled {...props} type="text" />
}

