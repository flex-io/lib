import _ from "lodash";
import styled, { ITheme } from "../styled";

export type IBaseTextInputProps = {
	_style?: IBaseTextInputStyle;
	disabled?: boolean;
};

interface ITextInput extends Omit<IBaseTextInputProps, "_style"> {
	theme: ITheme;
	[key: string]: any;
}

export interface IBaseTextInputStyle {
	_TextInput: (props: ITextInput) => any;
}



export const TextInputStyled = styled("input")<IBaseTextInputProps>`
	${(props) => {
		const sProps = _.omit(props, "children", "_style");
		return props._style?._TextInput({ ...sProps });
	}}
`;

