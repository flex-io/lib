import * as React from "react";
import { css } from "@emotion/core";
import { BaseButton, IBaseButtonStyle, IBaseButtonProps } from "@flexure/base-ui";

const scales = {
	small: `
	  padding: 5px 10px;
	  font-size: 14px;
	`,
	normal: `
	  padding: 10px 20px;
	  font-size: 16px;
	`,
	big: `
	  padding: 20px 30px;
	  font-size: 18px;
	`,
};

const kind = (outline: boolean) => (bg: string, color: string) => {
	const boxShadowColor = outline ? bg : "transparent";
	const backgroundColor = outline ? "transparent" : bg;

	return `
	  background: ${backgroundColor};
	  box-shadow: inset 0 0 0 1px ${boxShadowColor};
	  color: ${outline ? bg : color};
	  transition: all .3s;
  
	  &:hover {
		box-shadow: inset 0 0 0 1000px ${boxShadowColor};
		color: ${color};
	  }
	`;
};

type Kind = "primary" | "secondary" | "cancel" | "dark" | "gray";
type Kinds = Record<Kind, string>;

const kinds = (outline: boolean): Kinds => {
	const get = kind(outline);

	return {
		primary: get("#1FB6FF", "white"),
		secondary: get("#5352ED", "white"),
		cancel: get("#FF4949", "white"),
		dark: get("#273444", "white"),
		gray: get("#8492A6", "white"),
	};
};

const getScale = ({ scale = "normal" }: IBaseButtonProps) => scales[scale];
const getKind = ({ kind = "primary", outline = false }: IBaseButtonProps) => kinds(outline)[kind];

const style: IBaseButtonStyle = {
	_button: (props) => css`
		${getKind(props)};
		${getScale(props)};
		cursor: pointer;
		margin: 3px 5px;
		border: none;
		border-radius: 3px;
	`,
};

export const Button: React.FC<IBaseButtonProps> = (props) => {
	return (
		<BaseButton _style={style} {...props}>
			{props.children}
		</BaseButton>
	);
};
