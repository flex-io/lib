import { Flexite } from "./flexite";

export default Flexite;
export { IFlexTree, IFlexNode } from "./flexite";
export { FlexComponent, Flexton, IContextCallback } from "./flexton";

// export { Flexite } from "./core/flexite";
