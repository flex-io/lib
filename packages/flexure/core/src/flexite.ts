import { Flexton } from "./flexton";

// import { Builder } from "./internal";
var _ = require("lodash");

interface Properties {
	icon?: any;
	component?: string;
	name?: string;
	path?: string;
	isExact?: boolean;
}

export interface IFlexNode {
	icon?: any;
	component?: any;
	name: string;
	path?: string;
	isExact?: boolean;
	nodes?: IFlexNode[];
}
export interface IFlexTree {
	name: string;
	icon?: any;
	component: any;
	nodes?: IFlexNode[];
}

export abstract class IFlexite {
	abstract props: Properties;
	protected parent: IFlexite | any = undefined;

	public setParent(parent: IFlexite | any) {
		this.parent = parent;
	}

	public getParent(): IFlexite {
		return this.parent;
	}

	public abstract add(component: IFlexite): void;

	public abstract remove(component: IFlexite): void;

	public isComposite(): boolean {
		return false;
	}

	public abstract operation(): { [key: string]: any };
}

// export class Leaf extends Component {
// 	props: Properties = {};

// 	public operation(): { [key: string]: any } {
// 		return { leaf: "Leaf" };
// 	}
// }

export class FlexComposite extends IFlexite {
	props: Properties = {};
	protected children: IFlexite[] = [];

	public add(component: IFlexite): void {
		this.children.push(component);
		component.setParent(this);
	}

	public remove(component: IFlexite): void {
		const componentIndex = this.children.indexOf(component);
		this.children.splice(componentIndex, 1);

		component.setParent(null);
	}

	public isComposite(): boolean {
		return true;
	}

	public operation(): { [key: string]: any } {
		const results: any = this.children.length > 0 ? {} : undefined;
		this.children.forEach((child) => {
			_.assign(results, child.operation());
		});

		return { [`${this.props.name}Tree`]: { ...this.props, nodes: results } };
	}

	public initialize(arg: IFlexTree) {
		const builder = new Builder<FlexComposite>(this);
		this.setParent(builder.buildApp(arg));
	}
}

export const Flexite = new FlexComposite();

/**
 *
 *
 *
 *
 * 				BUILDER
 *
 *
 *
 *
 */

interface IBuilder {
	buildApp(args: any): void;
}

export class Builder<T extends IFlexite> implements IBuilder {
	private product: T;

	constructor(parent: T) {
		this.product = parent;
	}

	protected addApp(parent: IFlexite, child: IFlexNode) {
		const _child = new FlexComposite();
		let component: any = child.component ? Flexton.register(child.component) : undefined;
		_child.props = { ...child, component };

		child.nodes?.forEach((node) => {
			// note: Register the path to Flexton
			node.path && Flexton.registerPath({ ...node, component: node.component?.displayName });

			this.addApp(_child, node);
		});

		parent.add(_child);
		Flexton.registerContext(parent.operation());
	}

	public buildApp(tree: IFlexTree): T {
		this.product.props.name = tree.name;
		this.product.props.component = Flexton.register(tree.component);

		tree.nodes?.forEach((node) => {
			this.addApp(this.product, node);
		});
		Flexton.registerContext(this.product.operation());
		// console.log(this.product.operation())
		return this.product;
	}

	public getProduct(): T {
		const result = this.product;
		return result;
	}
}
